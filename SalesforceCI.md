# DEPLOY NO SALESFORCE
<img src="images/jenkins-salesforce.png" alt="Salesforce" style="width: 500px;"/>

Fala galera, beleza! :v:
Hoje aqui no blog estaremos vendo um pouco sobre Salesforce, e como podemos fazer Deploy no mesmo, Vamos lá ?! :runner::dash:

### FOCO DO POST
O objetivo desse post é fazer o Deploy no Salesforce.
  * Gerar pacote automático;
  * Fazer Test no Salesforce automático;
  * Fazer Deploy no salesforce automático.

Obs.: Nesse post não iremos mostrar como foi feito a análise de código, pois não é o foco do post, mas caso você queira usar algum, fique a vontade, se quiser uma sugestão, use o PMD, segue a **[Documentação](https://pmd.github.io/)** da ferramenta (fique a vontade na escolha da ferramenta, o PMD é apena uma sugestão ok?).

Segue abaixo um fluxo de como irá ficar, lembrando que o PMD não estará no post, mas colocamos ele no fluxo caso o mesmo for utilzado por vocês:
<center>
  <img src="images/DiagramPost.png" alt="Diagrama"/>
</center>

### FERRAMENTAS

Abaixo terão as ferramentas que iremos utilizar, e também uma breve explicação de cada uma delas:

<center>
  <img src="images/salesforce.png" alt="Salesforce" style="width: 150px;"/>
</center>
  * É um sistema para ajudar a gerenciar grandes volumes de informações em uma organização e melhora a comunicação entre os departamentos de serviço ao cliente. O mesmo é dividido em diferentes conjuntos de ferramentas chamadas de nuvens, e cada nuvem suporta uma parte diferente de seu plano geral de gerenciamento de relacionamento.

  Sandbox é um ambiente de teste que isola mudanças de código não testadas e experimentação definitiva do ambiente de produção ou repositório. Não afetando o ambiente de produção. Acesse os
  **[Tipos de Sandbox](https://help.salesforce.com/articleView?id=create_test_instance.htm&type=0)** se tiver mais interesse.

  Orgs nada mais é que o ambiente todo do  salesforce(como se fosse um repositório), ou seja o projeto todo.

  Para mais informações alguns links sobre a ferramenta:  
    1. **[Documentação](https://developer.salesforce.com/docs/)**  
    2. **[Treinamento salesforce](https://trailhead.salesforce.com/pt-BR)**  
    3. **[Para mais informações](http://www.salesforcebrasil.com.br/o-que-e-salesforce/)**

<center>
  <img src="images/gitlab.png" alt="Gitlab" style="width: 150px;"/>
</center>
  * É uma ferramenta para gerenciamento de repositório, muito parecido com o github, porém com o gitlab é possivel subir o serviço localmente. No gitlab ficará o código onde o desenvolvedor poderá manipular, através das branchs.
  Para mais informações acesse a **[Documentação](https://docs.gitlab.com/)** da ferramenta.

  Pode ser utilizado qualquer ferramenta para gerenciamento de código (Bitbucket, Github, etc.), porém iremos utilizar o Gitlab.

<center>
  <img src="images/git.png" alt="Git" style="width: 150px;"/>
</center>
  * Uma ferramenta para versionamento de código, com ele você consegue manipular o seu repositório com comando do próprio git.
  Para mais informações acesse a **[Documentação](https://git-scm.com/doc)** da ferramenta.

<center>
  <img src="images/jenkins.png" alt="Jenkins" style="width: 150px;"/>
</center>
  * Uma ferramenta que permite a automatização do CI / CD.
  Com o jenkins conseguimos automatiazar funcionalidades que tem uma grande periodicidade de execuação, como por exemplo, o build (que é nada mais nada menos que, uma versão copilado do software, que contém um conjunto de recurso que poderão intergrar o produto final), testes, análise do código. Essas tarefas são executadas sempre que ocorrer um evento (como Merge Request, Push, etc.)

    * Pipeline: É um conjunto de plugins que suporta a implementação e entrega continua (CD), a pipeline fornece um conjunto extenso de ferramentas para modelar da melhor forma possível. Fazendo com que execute várias tarefas uma na sequência da outra, podendo ser executadas automaticamente ou não.

    * Modelo de Pipeline:
    <img src="images/pipeline.png" alt="Git" style="width: 1000px;"/>

    * Job: Cada "quadrado verde" da imagem acima, é uma tarefa na qual você configura algo para ser executado, código em shell por exemplo.

    Para mais informações acesse a **[Documentação](https://jenkins.io/doc/)** da ferramenta.

<h2 style="text-align: center; color: purple;">FORCE-DEV-TOOL</h2>

  * Uma ferramenta em linha de comando 3rd party que suporta o ciclo de vida de Aplicações Salesforce.
  Para mais informações acesse a **[Documentação](https://github.com/amtrack/force-dev-tool)** da ferramenta.  

Chega de tanta ~~enrolação~~ explicação :sleeping: e vamos para a prática! :muscle:

### MÃO NA MASSA

Bom lembrar que o SO que iremos utilizar aqui é o Linux.

Primeiramente vamos configurar as ferramentas que foi citadas acima, caso deseja utilizar o **[Docker](https://docs.docker.com/)** fique a vontade.

#### Gitlab

Crie uma conta no Gitlab, caso você já tenha, crie um repositório.

Fiquem a vontade de usar outro versionamento de código. :relaxed:

#### Salesforce

Faça o cadastro no **[Salesforce](https://developer.salesforce.com/signup?d=70130000000td6N)**.

Crie um projeto seguindo os passos abaixo:

Ao logar, em Getting Started coloque Add App, coloque os dados necessários, Segue abaixo o exemplo:

<img src="images/project_salesforce.png" alt="project_salesforce" style="width: 550px;"/>

Projeto criado, agora só iniciarmos em Go To My App.
Irá exibir uma mensagem perguntando se você gostaria de iniciar um Tour pelo Salesforce, se quiser fazer fiquem a vontade, apenas clicando em "Start Tour", caso contrário, "No Thanks".

Vamos criar um objeto, vá em Setup (canto direito superior da tela), em Quick Links >> Tools >> New custom object, preencha os campos necessários (vamos criar um campo tipo texto). Segue o exemplo na imagem abaixo:

<img src="images/object_salesforce.png" alt="project_salesforce" style="width: 550px;"/>

Salve o objeto criado em "Save".

Ótimo, aqui aprendemos a criar um Projeto e um Objeto, caso queira aprofundar-se e criar Triggers, Classes fique a vontade. E caso precise de ajuda, tem um treinamento no link passado anteriomente ao explicar a ferramenta (ajuda bastante! Vai por mim! rs :stuck_out_tongue:)

:warning: Importante :warning:

Salve o usuário e a senha, e siga os passos abaixo para resgatar o token(o mesmo é enviado para o seu email):

Estando na Home, vá em Seu nome >> My Settings >> Personal >> Reset My Security Token >> Reset Security Token.


#### Force-dev-tool

Antes de fazer a instalação do force-dev-tool, faça a instalação do NPM, assim que o npm estiver instalado, faça a instalação do force-dev-tool, segue os comandos abaixo:

* Instalação:
  > sudo apt-get install npm  
  > sudo apt install nodejs-legacy   
  > npm install --global force-dev-tool


Após a criação do projeto no Salesforce, a criação do repositório, e a instalação do force-dev-tool, vá ao Terminal, e siga os seguintes passos:

  > git clone {seu_repo}

Assim que você tiver seu repositório local, use o comando do force-dev-tool para fazer o retrieve (pull) do projeto que foi criado no salesforce. Siga os passos abaixo:

Com o comando a seguir será possivel acessar remotamente a org do Salesforce:
> force-dev-tool remote add {nome_ambiente} {seu_usuario_saleforce} {senha+token_salesforce} {url_salesforce}     

O comando abaixo faz com que certifiquemos que o login foi realizado com sucesso(se você tem certeza que utilizou o login corretamente o mesmo não é necessário, o comando abaixo é opcional):
>force-dev-tool login {nome_ambiente}   

Com o comando a seguir ele cria um ambiente local, com as configurações necessárias para poder criar o pacote.
>force-dev-tool fetch --progress {nome_ambiente}   

Agora vamos criar o pacote em XML para atráves da sua org criada no Salesforce:
>force-dev-tool package -a {nome_ambiente}   

Com o comando abaixo será possivel "baixar" todo o seu código, através do pacote criado anteriormente, siga abaixo:
>force-dev-tool retrieve {nome_ambiente}  

Assim que o retrieve for feito dentro do repositorio local, faça o push no gitlab (ou no versionamento do código que você estiver utilizando).

> git push origin {sua_branch} ou git push

Tudo pronto para iniciarmos a automação! :yum:

#### Jenkins

* Instalação:
**[Documentação](https://jenkins.io/doc/)**  
Como eu disse anteriormente, fique á vontade na escolha de Docker ou se quiser fazer local mesmo, lembrando que caso escolha o Docker, lembre-se de fazer a instalação do force-dev-tool no seu container.   

* Configurações Gerais:
  * Manage Jenkins >> Configure System    

    Configure as variaveis de ambiente, com as senhas e as propriedades globais, no Mask Passwords você precisará colocar a senha que é utilizada no Salesforce, e as Propriedades globais com a url e o usuário. Segue o exemplo abaixo:<br /><br />
    <img src="images/pwd_salesforce.png" alt="pwd_salesforce" style="width: 550px;"/>
    <img src="images/variavel_salesforce.png" alt="variveis" style="width: 550px;"/>  

  * Manage Jenkins >> Global Tool Configuration
    Configure o Ant, para que possamos executar o PMD. <br /><br />
    <img src="images/configuration_ant.png" alt="ant" style="width: 550px;"/> <br /><br />

Lembrando que a senha que você precisará utilizar tem que conter o token junto, portanto a Mask Passwords ficará (Senha+Token). Nessa altura do campeonato, provavelmente você já tem esse token, caso não tenha, siga os passos que foram passados anteriormente.

* Configurações dos Jobs:  
  Os nomes dos jobs foi da minha escolha ok? Podem colocar o nome que você preferir.

**1. Codepull:**  
  O job terá a finalidade de fazer o pull do repositorio. E arquivar o mesmo, para que possamos utiliza-lo nos proximos jobs, isso será importante. Segue abaixo as configurações necessárias:

  * Faça o pull do repositório que você irá utilizar.  
  <img src="images/configuration_git.png" alt="configuration_git" style="width: 550px;"/>

  * Trigge para com o próximo job conforme mostra a imagem abaixo:
  <img src="images/trigger_job_codepull.png" alt="trigger" style="width: 550px;"/>  

**2. CreatePackage:**  
  Esse job irá gerar o pacote automatico através do comando em shell, o mesmo pacote que será feito o Deploy. Garanta que o force-dev-tool esteja instalado na sua máquina, se não instalou, instale, caso tenha dúvida de como instalar, suba um pouco o post, é bem simples. Segue abaixo, o que você irá precisar configurar no mesmo.

  * Lembre-se de colocar os "**This project is parameterized**".
  * Faça o pull novamente do repositório, pois iremos precisar, pelo menos nesse job, para que possamos criar o pacote.
  * Bom, enfim chegamos na principal função desse job, a criação do pacote. Para isso utilize no "**Execute shell**" o comando abaixo.
  > git diff -r 'HEAD~' 'HEAD' | force-dev-tool changeset create scr_diff

  Aqui estamos fazendo o diff entre os dois últimos commits, pois é o necessário para criarmos o pacote, e com o comando _force-dev-tool_ criamos o pacote.

  * Arquive todos os arquivos com "**Archive the artifacts**" e enfim trigge com o job seguinte. Como mostra a imagem abaixo:

  <img src="images/trigger_job_cp.png" alt="trigger" style="width: 550px;"/>

Caso você estiver utilzando análise de código, lembre de configurar o mesmo antes de fazer o deploy.

  **3. Deploy:**  
  Como o nome do job já diz, a função desse nada mais nada menos que fazer o deploy no ambiente do salesforce.

  * Faça o "**Copy artifacts from another project**" do job de CreatePackge, pois o deploy será feito utilizando o pacote que foi criado. Para isso utilize o comando abaixo:

  > force-dev-tool remote add {nome_ambiente} $USER_SALESFORCE $PWD_SALESFORCE $URL_SALESFORCE  
  force-dev-tool login {nome_da_ambiente}  
  force-dev-tool deploy {nome_do_ambiente} -d config/deployments/scr_diff

  Os dois primeiros comandos foram explicados mais acima, eles permitem que sejam possiveis a realização de login no Salesforce, o terceiro comando executado, permite que seja feito o deploy no ambiente do Salesforce.

  * Trigge para o próximo job, para ser mais exato o de Test, use apenas o "**Current build parameters**".


**4. Test:**  
  Esse job tem como finalidade fazer o testes das APEX (classes) do Salesforce.

  * Mais uma vez e por último, não se esqueça de colocar os "**This project is parameterized**".
  * Faça o "**Copy artifacts from another project**" do job de CreatePackge.
  * E por último use o comando abaixo para que possa ser feito os teste das APEX:
  > force-dev-tool remote add {nome_ambiente} $SFDC_USER_DEV $SFDC_PWD_DEV $SFDC_SERVER_TEST_URL
  force-dev-tool login {nome_ambiente}
  force-dev-tool test {nome_ambiente}

Como foi feito no job anterior, é feito o login no ambiente do Salesforce, e executamos o Test como mostra o comando acima.

Bom galera, é isso! :blush: Deploy feito com sucesso, agora é só correr para o abraço. Agradeço a todos, dúvidas, sugestões, feedbacks, são muito bem-vindos. :smile:

Até a próxima! :wave:
